//
// Created by Peter Quaranta on 11/26/17.
//

#include "MFQS.h"
#include "RoundRobin.h"
#include "FCFS.h"

using namespace std;

MFQS::MFQS(vector<Process*> processes, int quantum, int tiers, int ageTime, bool debug) : Scheduler(processes), quantum(quantum),
                                                                                                    tiers(tiers),
                                                                                                    ageTime(ageTime) {
    if (debug){
        #define DEBUG
    }
    MFQS::execute();

}



void MFQS::execute() {
    std::cout << "Running multilevel feedback queue with quantum = " << quantum << ", " << tiers << " levels, " << " and an age time of " << ageTime << "\n";
    bool stop = false;
    int initialQuantum = quantum;


    //Blank vector to use for constructors
    vector<Process*> temp;
    for (int i = 0; i < tiers - 1; i++){
        mfqs.push_back(new RoundRobin(temp,quantum));
        quantum = quantum * 2;
    }
    //Last level is always FCFS
    mfqs.push_back(new FCFS(temp));

    vector<Process*> processes = this->getProcesses();
    int currentIndex = 0;

    while(!stop){
        //Get processes at the current time
        //Add them all to the run queue of the first level
        while (currentIndex < processes.size() && processes[currentIndex]->getArrival() <= currentTime) {
            mfqs[0]->pushRunQueue(processes[currentIndex]);
            currentIndex++;
        }

        // Handle all aging processes

        mfqs[tiers - 1]->ageProcesses(currentTime, ageTime);
        list<Process*> agingProcesses = mfqs[tiers - 1]->popToAge();
        for (int i = 0; i < agingProcesses.size(); i++) {
            Process* p = agingProcesses.front();
            mfqs[tiers - 2]->addToRunQueue(p);
            agingProcesses.pop_front();
        }




        //Find the first runnable proccess
        int foundIndex = -1;

        int i = 0;
        while (foundIndex < 0 && i < tiers) {
            if (mfqs[i]->hasRunnable()) {
                foundIndex = i;
            } else {
                i++;
            }
        }

        int timeRan;

        // If we found a scheduler with a process available to run
        if (foundIndex > -1) {
            #ifdef DEBUG
                std::cout << "Running on level " << foundIndex + 1 <<  "\n";
            #endif
            // Run the process
            timeRan = mfqs[foundIndex]->runOnce();



            // Update all wait times and current times
            for (int i = 0; i < tiers; i++) {
                mfqs[i]->updateCurrentTime(timeRan);
            }




            updateCurrentTime(timeRan);

            // Handle any demotion
            if (mfqs[foundIndex]->hasDemotion()) {
                Process* demotedProcess = mfqs[foundIndex]->popDemotion();
#ifdef DEBUG
                std::cout << "Demoting process " << demotedProcess->getP_id() << "\n";
#endif
                mfqs[foundIndex+1]->pushRunQueue(demotedProcess);
            }


            std::cout << "\n";

        } else {
            if (currentIndex == processes.size()) {
                stop = true;
            } else {
                int nextTime = processes[currentIndex]->getArrival();

                int timeWaited = nextTime - currentTime;
                std::cout << "No process yet, waiting " << timeWaited << " seconds for a new arrival\n";
                currentTime = nextTime;

                // Update all wait times and current times

                for (int i = 0; i < tiers; i++) {
                    mfqs[i]->updateCurrentTime(timeWaited);
                }

            }
        }


    }

    cout << "Getting Stats for Run" << endl;
    unsigned long long x = 0;
    for (int i = 0; i < tiers; i++) {
        x += mfqs[i]->getTotalTurnAroundTime();
    }
    cout << "Processes Scheduled: " << processes.size() << endl;
    cout << "Average Turnaround: " << x / (unsigned long long)processes.size() << endl;
    unsigned long long y = 0;
    for (int i = 0; i < tiers; i++) {
        y += mfqs[i]->getTotalWaitTime();
    }
    cout << "Average Wait Time: " << y / (unsigned long long)processes.size() << endl;


}

