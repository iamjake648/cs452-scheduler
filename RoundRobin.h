//
// Created by Peter Quaranta on 11/23/17.
//

#ifndef CS452_SCHEDULER_ROUNDROBIN_H
#define CS452_SCHEDULER_ROUNDROBIN_H

#include "Scheduler.h"
#include <iostream>

class RoundRobin : public Scheduler {
private:
    int quantum;
    vector<Process*> finished;
public:
    RoundRobin(vector<Process*> processes, int quantum);
    void execute();
    int runOnce();
    vector<Process*> getFinished();
};


#endif //CS452_SCHEDULER_ROUNDROBIN_H
