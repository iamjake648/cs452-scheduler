//
// Created by Peter Quaranta on 11/23/17.
//

#include "Scheduler.h"
#include <iostream>

Scheduler::Scheduler(vector<Process *> processes) : processes(processes), currentTime(0), totalWaitTime(0), totalTurnAroundTime(0) {}

vector<Process*> Scheduler::getProcesses() {
    return processes;
}

vector<Process*> Scheduler::getProcess(int time) {
    vector<Process*> result;
    while (processes.size() > 0 && processes.front()->getArrival() <= time) {
        Process* p = processes.front();
        p->setWaitTime(time - p->getArrival());
        result.push_back(p);
        processes.erase(processes.begin());
    }
    return result;
}

void Scheduler::pushRunQueue(Process* p) {
    runQueue.push_back(p);
}

vector<Process*> Scheduler::getFinishedForPrint() {
    return finishedForPrint;
}

void Scheduler::addToRunQueue(Process* p) {
    if (runQueue.size() == 0) {
        runQueue.push_back(p);
    } else {
        list<Process*>::reverse_iterator it, toInsert;
        toInsert = runQueue.rbegin();
        it = runQueue.rbegin();
        while (it != runQueue.rend() && p->compareTo((*it)) < 0) {
            it++;
        }
        runQueue.insert(it.base(), p);
    }
}

bool Scheduler::hasRunnable() {
    return !runQueue.empty();
}

void Scheduler::printRunQueue() {
    std::cout << "Current time: " << currentTime << "\n";
    list<Process*>::iterator it;
    for (it = runQueue.begin(); it != runQueue.end(); it++) {
        (*it)->print();
        std::cout<<"\n";
    }
}

unsigned long long Scheduler::getTotalWaitTime() {
    return totalWaitTime;
}

unsigned long long Scheduler::getTotalTurnAroundTime() {
    return totalTurnAroundTime;
}

void Scheduler::execute() {}

int Scheduler::runOnce() {}

void Scheduler::updateCurrentTime(int time) {
    currentTime += time;
}

void Scheduler::updateWaitTimes(int timeElapsed) {
    list<Process*>::iterator it;
    for (it = runQueue.begin(); it != runQueue.end(); ++it) {
        (*it)->addWaitTime(timeElapsed);
    }
}

int Scheduler::getCurrentTime() {
    return currentTime;
}

list<Process *> Scheduler::getRunQueue() {
    return runQueue;
}

Process* Scheduler::popDemotion() {
    Process* p = toDemote.front();
    toDemote.pop_front();
    return p;
}

bool Scheduler::hasDemotion() {
    return !toDemote.empty();
}

int Scheduler::runQueueSize() {
    return runQueue.size();
}

void Scheduler::ageProcesses(int currentTime, int timeToAge) {

    bool stop = false;
    while (!runQueue.empty() && !stop) {
        int timeInQueue = currentTime - runQueue.front()->getScehdulerArrival();
        if (timeInQueue >= timeToAge) {
            runQueue.front()->setSchedulerArrival(currentTime + timeToAge);
            toAge.push_back(runQueue.front());
            runQueue.pop_front();
        } else {
            // This is true because processes are in order of scheduler arrival time
            stop = true;
        }
    }
}

list<Process*> Scheduler::popToAge() {
    list<Process*> agingProcesses;
    while(!toAge.empty()) {
        Process* p = toAge.front();
        agingProcesses.push_back(p);
        toAge.pop_front();
    }
    return agingProcesses;
}