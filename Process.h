//
// Created by Peter Quaranta on 11/16/2017.
//

#ifndef UNTITLED_PROCESS_H
#define UNTITLED_PROCESS_H


class Process {

protected:
    int p_id, endtime, burst, arrival, schedulerArrival, priority, deadline, i_o, timeRan, waitTime, turnaroundTime, timePromoted, timeDemoted;

public:
    Process(int p_id, int burst, int arrival, int priority, int deadline, int i_o);

    virtual ~Process();

    int compareTo(Process* p);

    int runAndPrint(int time, int runTime);

    int run(int time);

    void print();

    int getP_id() const;

    void setP_id(int p_id);

    int getBurst() const;

    void setBurst(int burst);

    int getArrival() const;

    void setArrival(int arrival);

    int getPriority() const;

    void setPriority(int priority);

    int getDeadline() const;

    void setDeadline(int deadline);

    int getI_o() const;

    void setI_o(int i_o);

    int getWaitTime() const;

    void setWaitTime(int waitTime);

    void addWaitTime(int waitTime);

    int getTurnaroundTime() const;

    void setTurnaroundTime(int turnaroundTime);

   // double run(int seconds);

    double getEndTime() const;

    void setEndTime(int endTime);

    int getTimeRan() const;

    void addTimeRan(int time);

    void setSchedulerArrival(int time);

    int getScehdulerArrival() const;

    void setTimePromoted(int time);

    int getTimePromoted() const;

    void setTimeDemoted(int time);

    int getTimeDemoted() const;

    int runAndPrintWithIO(int time, int runTime);

    int runRealTime(int time);
};


#endif //UNTITLED_PROCESS_H
