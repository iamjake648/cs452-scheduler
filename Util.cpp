//
// Created by Jacob Schultz on 11/17/17.
//

#include "Util.h"
#include "Process.h"

Util::Util(char* filename, string type) {
    this->schedulingAlgorithim = type;
    vector<Process*> processes = readFileToVector(filename,type);

    sort(processes.begin(), processes.end(), [this](Process *p1, Process *p2) {
        return p1->getArrival() < p2->getArrival();
    });

    //printParsedProcceses(processes);
    this->processes = processes;
}

vector<Process*> Util::readFileToVector(char* filename, string algorithim) {
    vector<string> my_arr;
    vector<Process*> processes;
    ifstream dict_file(filename);
    string line;
    int totaltime = 0;
    while(getline(dict_file, line)) {
        //Split up the line by tokens
        vector<string> tokens = split(line, '\t');
        //Don't include the first line
        if (tokens[0] != "Pid"){
            int pid = stoi(tokens[0]);
            int burst = stoi(tokens[1]);
            int arrival = stoi(tokens[2]);
            int priority = stoi(tokens[3]);
            int deadline = stoi(tokens[4]);
            int io = stoi(tokens[5]);
            //Dont' add a process that doesn't have a deadline for a real time queue
            if (algorithim.compare("realtime") == 0){
                //Make sure the deadline is not negative, and deadline is not less than arrival
                if (deadline > 0 && deadline > arrival) {
                    //Tasks with a burst time + arrival time that is greater than the deadline also won't run
                    if ((arrival + burst) < deadline){
                        Process *p = new Process(pid, burst, arrival, priority, deadline, 0);
                        processes.push_back(p);
                    }
                }
            } else if (algorithim.compare("MFQS") == 0) {
                Process *p = new Process(pid, burst, arrival, priority, deadline, 0);
                if (p->getBurst() >= 0) {
                    processes.push_back(p);
                    totaltime += p->getBurst();
                }
            } else if (algorithim.compare("WHS") == 0){
                Process *p = new Process(pid, burst, arrival, priority, deadline, io);
                if (p->getBurst() >= 0 && p->getArrival() >= 0 && priority < 100 && io >= 0) {
                    processes.push_back(p);
                }
            }
        }
        my_arr.push_back(line);
    }
    std::cout << totaltime << "\n";
    return processes;
}

vector<string> Util::split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

void Util::printParsedProcceses(vector<Process *> processes) {
    for (int i = 0; i < processes.size(); i++){
        cout << "PID: " + to_string(processes[i]->getP_id()) + " Burst: " + to_string(processes[i]->getBurst()) + " Arrival: " + to_string(processes[i]->getArrival()) + " Priority: " + to_string(processes[i]->getPriority()) + " Deadline: " + to_string(processes[i]->getDeadline()) << endl;
    }
}

vector<Process *> Util::getSortedProcesses() {
    return this->processes;
};

