#include <iostream>
#include "Util.h"
#include "Process.h"
#include "Scheduler.h"
#include "RoundRobin.h"
#include "RealTime.h"
#include "WHS.h"
#include "MFQS.h"

//#define DEBUG
int main(int argc, char *argv[]) {
    string schedulerType;
    string numberOfQueues;
    string timeQuantum;
    string ageTime;
    string debugging;
    string hardSoft;

    //Get the scheduler type from the user
    cout << "Enter the Scheduler Type(1,2,3): 1 : Real Time 2 : MFQS 3: WHS " << endl;
    getline(cin, schedulerType);

    cout << "Would you like debugging? (y/n)" << endl;
    getline(cin, debugging);

    bool debug = false;
    if (debugging == "y"){
        debug = true;
    }

    if (schedulerType == "1"){
        cout << "Would you like a soft or hard env? (s/h)" << endl;
        getline(cin, hardSoft);
        bool hard = false;
        if (hardSoft == "h"){
            hard = true;
        }
        //Create the real time scheduler
        Util* u = new Util(argv[1],"realtime");
        Scheduler* s = new RealTime(u->getSortedProcesses(), debug, hard);

    } else if (schedulerType == "2") {
        cout << "Enter the number of Queues (2-5)" << endl;
        getline(cin, numberOfQueues);
        //Get the time quantum
        cout << "Enter the time Quantum" << endl;
        getline(cin, timeQuantum);
        cout << "Enter the Age Time" << endl;
        getline(cin, ageTime);

        //Get variables needed for the queue
        int queueCount = stoi(numberOfQueues);
        int quantum = stoi(timeQuantum);
        int age = stoi(ageTime);

        //Create the scheduler
        Util* u = new Util(argv[1],"MFQS");
        Scheduler* s = new MFQS(u->getSortedProcesses(), quantum, queueCount, age, debug);
    } else if (schedulerType == "3") {
        //Get the time quantum
        cout << "Enter the time Quantum" << endl;
        getline(cin, timeQuantum);
        cout << "Enter the Age Time" << endl;
        getline(cin, ageTime);

        //Get variables needed for the queue
        int quantum = stoi(timeQuantum);
        int age = stoi(ageTime);

        //Create the scheduler
        Util* u = new Util(argv[1],"WHS");
        Scheduler* s = new WHS(u->getSortedProcesses(), quantum, age, debug);
    }

    return 0;
}