//
// Created by Jacob Schultz on 11/24/17.
//

#include "FCFS.h"
#include <iostream>

FCFS::FCFS(vector<Process *> processes) : Scheduler(processes){
//    int time = 0;
//    bool stop = false;
//    vector<Process*> runQueue;
//    while(!stop){
//        //Get a list of processes arriving at the current time
//        vector<Process *> processesAtTime = getProcess(time);
//        //Add any processes arriving at this time to the run queue
//        if (processesAtTime.size() > 0){
//            //Add them all to the end of the queue
//            for (int i = 0; i < processesAtTime.size(); i++){
//                //No need to sort for first come first serve, run as they arrive
//                runQueue.push_back(processesAtTime[i]);
//            }
//        }
//        //Make sure there is something to run in the queue
//        if (runQueue.size() > 0){
//            //check if the process has anytime left
//            int processTimeLeft = runQueue[0]->run(1);
//            //Check if it's fnished running
//            if (processTimeLeft == 0){
//                //Remove from the run queue
//                //#TODO REMOVE FROM QUEUE
//                //Set end time to the current time
//                runQueue[0]->setEndTime(time);
//            }
//        }
//        //Timestamp the wait time on the rest in the run queue
//        if (runQueue.size() > 1){
//            //Timestamp everything but the first process
//            for (int i = 1; i < runQueue.size(); i++){
//                //Increment wait time
//                runQueue[i]->setWaitTime(runQueue[i]->getWaitTime() + 1);
//            }
//        }
//        time++;
//    }
}

//When executing in the FCFS queue, everything should be in the queue, so execute in order
void FCFS::execute() {
    int time = 0;
    list<Process*>::iterator it;
    //Loop through everything in the run queue
    for (it = runQueue.begin(); it != runQueue.end(); it++){
        //Print out the process
        std::cout << " Running Process " << (*it)->getP_id() << " from " << time << " to " << time + (*it)->getBurst() << std::endl;
        //Set the end time
        (*it)->setEndTime(time + (*it)->getBurst());
        //Remove from the run queue
        runQueue.remove(*it);
        int timeForWait = time + (*it)->getBurst();
        //set the wait time for the rest in the list
        //this->updateWaitTimes(timeForWait);
        //Set the new current time
        time = timeForWait;
    }
}

int FCFS::runOnce() {
    //Pop off a process
    Process* p = runQueue.front();
    //Run the process - run for the remaining duration
    int timeLeft = p->getBurst() - p->getTimeRan();
    p->runAndPrint(currentTime,timeLeft);

    totalWaitTime += p->getWaitTime();
    totalTurnAroundTime += (currentTime + timeLeft) - p->getArrival();
    runQueue.pop_front();
    return timeLeft;
}

vector<Process *> FCFS::getFinished() {
    return finished;
}
