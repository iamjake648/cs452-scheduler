//
// Created by Peter Quaranta on 11/26/17.
//

#ifndef CS452_SCHEDULER_MFQS_H
#define CS452_SCHEDULER_MFQS_H

#include "Scheduler.h"
#include <string>
#include <math.h>

using std::string;
class MFQS: public Scheduler{

public:
    MFQS(vector<Process*> processes, int quantum, int tiers, int ageTime, bool debug);
    void execute();
    bool hasAgingProcesses();
    void ageProcesses();

protected:
    int quantum, tiers, ageTime;
    vector<Scheduler*> mfqs;

    list<Process*> toDemote;
    list<Process*> toAge;

    void printStats();
};


#endif //CS452_SCHEDULER_MFQS_H
