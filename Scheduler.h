//
// Created by Peter Quaranta on 11/23/17.
//

#ifndef CS452_SCHEDULER_SCHEDULER_H
#define CS452_SCHEDULER_SCHEDULER_H

#include "Process.h"
#include <vector>
#include <list>

using std::vector;
using std::list;

class Scheduler {
protected:
    list<Process*> toDemote;
    list<Process*> toAge;
    list<Process*> runQueue;

    // Printing stats
    unsigned long long totalWaitTime;
    unsigned long long totalTurnAroundTime;

    int currentTime;
    //Used in the real time queue
    vector<Process*> processes;
    vector<Process*> finishedForPrint;
public:
    Scheduler(vector<Process*> processes);

    vector<Process*> getProcesses();
    void setProcesses(vector<Process*> processes);

    // Pop all processes up to the provided time and return the vector containing those processes
    vector<Process*> getProcess(int time);

    void addToRunQueue(Process* p);
    void pushRunQueue(Process* p);
    void printRunQueue();

    vector<Process*> getFinishedForPrint();

    list<Process*> getAgingProcesses();

    bool hasRunnable()
;
    virtual void execute();
    virtual int runOnce();

    int runQueueSize();

    void updateCurrentTime(int time);
    void updateWaitTimes(int timeElapsed);
    void ageProcesses(int currentTime, int timeToAge);

    int getCurrentTime();

    list<Process*> getRunQueue();
    list<Process*> popToAge();
    Process* popDemotion();
    bool hasDemotion();

    unsigned long long getTotalTurnAroundTime();

    unsigned long long getTotalWaitTime();
};


#endif //CS452_SCHEDULER_SCHEDULER_H
