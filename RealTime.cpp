//
// Created by Jacob Schultz on 11/23/17.
//

#include "RealTime.h"
#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include <fstream>
#include <algorithm>


using namespace std;

RealTime::RealTime(vector<Process*> processes, bool debug, bool hard) : Scheduler(processes){
    if (debug){
        #define DEBUG
    }
    RealTime::execute(hard);
    printStats();
}

void RealTime::printGanntChart(vector<Process*> processes){
//    for (int i = 0; i < processes.size(); i++){
//        cout << "End Time: " << processes[i]->getEndTime() << endl;
//    }
}

void RealTime::printStats(){
    long waitTotal = 0;
    long turnAroundTotal = 0;
    long totalNumberScheduled = 0;
    cout << "Getting Average Stats for Run" << endl;
    for (int i = 0; i < finishedForPrint.size(); i++){
        //Check if it ran
        if (finishedForPrint[i]->getEndTime() > 0){
            totalNumberScheduled++;
            waitTotal += finishedForPrint[i]->getWaitTime();
            turnAroundTotal += (finishedForPrint[i]->getEndTime() - finishedForPrint[i]->getArrival());
        }
    }
    cout << "Processes Scheduled: " << totalNumberScheduled << endl;
    cout << "Average Turnaround: " << turnAroundTotal / totalNumberScheduled << endl;
    cout << "Average Wait Time: " << waitTotal / totalNumberScheduled << endl;

}


void RealTime::execute(bool hard) {
    int time = 0;
    bool stop = false;
    vector<Process *> runQueue;
    //Keep track of how many processes there are total to know when to stop
    int processCount = processes.size();
    //Keep track of how many processes have come in so far
    int currentProcessCount = 0;

    while (!stop){
       // printf("PCount: %d Time: %d\n", currentProcessCount, time);
        //Get a list of the processes arriving at the current time
        vector<Process *> processesAtTime = getProcess(time);
        //If there are some processes at this current time
        if (processesAtTime.size() > 0){
            //Add all of the procceses at the current time to the run queue
            bool shouldSort = false;
            for (int i = 0; i < processesAtTime.size(); i++){
                if (time + processesAtTime[i]->getBurst() < processesAtTime[i]->getDeadline()){
                    runQueue.push_back(processesAtTime[i]);
                    shouldSort = true;
                } else {
                    if (hard){
                        cout << "Exiting at Process: " << processesAtTime[i]->getP_id() << " Because it's not able to complete before deadline in hard env" << endl;
                        exit(0);
                    }
                }
                //Increment the count of processes that have come in
                currentProcessCount++;
            }
            if (shouldSort){
                //Sort the run queue by deadline - closest deadlien first
                sort(runQueue.begin(), runQueue.end(), [this](Process *p1, Process *p2) {
                    return p1->getDeadline() < p2->getDeadline();
                });
            }
        }
        //Check if there is anything to run
        if (runQueue.size() > 0){
            int endTime = time + 1;
            #ifdef DEBUG
                cout << " Running Process: " << runQueue[0]->getP_id() << " From Time: " << time << "s To Time:" << endTime << "s" << endl;
            #endif
            int processTimeLeft = runQueue[0]->run(1);
            //If the process is finished running
            if (processTimeLeft == 0){
                //It's finished so add to array for stats
                finishedForPrint.push_back(runQueue[0]);
                //Remove from the run queue
                runQueue.erase(runQueue.begin());
                //Set end time to the current time
                runQueue[0]->setEndTime(time + 1);
                #ifdef DEBUG
                    cout << " Process Id: " << runQueue[0]->getP_id() << " Finished at: " << time +1 <<"s" <<  endl;
                #endif
            }

            //Timestamp the wait time on the rest in the run queue
            if (runQueue.size() > 1){
                //Timestamp everything but the first process
                for (int i = 1; i < runQueue.size(); i++){
                    //Increment wait time
                    runQueue[i]->setWaitTime(runQueue[i]->getWaitTime() + 1);
                }
            }
            //Increase the time to run the next processes
            time++;
        } else {
            //Check if there are any other processes that haven't arrived
            if (currentProcessCount < processCount){
                time++;
            } else {
                stop = true;
            }
        }


    }
}
