//
// Created by Jacob Schultz on 11/24/17.
//

#ifndef CS452_SCHEDULER_FCFS_H
#define CS452_SCHEDULER_FCFS_H


#include "Scheduler.h"

class FCFS : public Scheduler {
private:
    vector<Process*> finished;
public:
    FCFS(vector<Process*> processes);
    vector<Process*> getFinished();
    int runOnce();
    void execute();
};


#endif //CS452_SCHEDULER_FCFS_H
