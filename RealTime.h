//
// Created by Jacob Schultz on 11/23/17.
//

#ifndef CS452_SCHEDULER_REALTIME_H
#define CS452_SCHEDULER_REALTIME_H


#include "Scheduler.h"
#include "Process.h"

class RealTime : public Scheduler {
private:
public:
    RealTime(vector<Process*> processes, bool debug, bool hard);

    void execute(bool hard);
    void printGanntChart(vector<Process*> processes);

    void printStats();
};


#endif //CS452_SCHEDULER_REALTIME_H
