//
// Created by Jacob Schultz on 11/17/17.
//

#ifndef CS452_SCHEDULER_QUEUE_H
#define CS452_SCHEDULER_QUEUE_H

#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include <fstream>
#include "Process.h"

using namespace std;

class Util {
public:
    Util(char* filename, string type);
    string schedulingAlgorithim;
    vector<string> split(const string &s, char delim);
    void printParsedProcceses(vector<Process*> processes);
    vector<Process*> getSortedProcesses();
private:
    vector<Process*> readFileToVector(char* filename, string algorithim);
    vector<Process*> processes;


};


#endif //CS452_SCHEDULER_QUEUE_H
