//
// Created by Jacob Schultz on 11/29/17.
//

#ifndef CS452_SCHEDULER_WHS_H
#define CS452_SCHEDULER_WHS_H

#include "Scheduler.h"
#include <map>

using namespace std;

class WHS : public Scheduler {
public:
    WHS(const vector<Process *> &processes, int quantum, int ageTime, bool debug);
    void execute();
    map<int, list<Process*>> returnQueue();

protected:
    int quantum, ageTime;
    //Store a list of the priorities
    map<int, list<Process*>> priorityQueuesHighBand;
    map<int, list<Process*>> priorityQueuesLowBand;
    bool containsKey(int val);
    list<Process*> getValueFromKey(int val);

    void addToBand(int level, Process *p);
    void deleteKeyFromMap(int val);

    void pushBackTiebreakers(list<Process *>* listToAddTo, Process *p);
};


#endif //CS452_SCHEDULER_WHS_H
