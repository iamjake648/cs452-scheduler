//
// Created by Peter Quaranta on 11/16/2017.
//

#include "Process.h"
#include <iostream>

// Constructor
Process::Process(int p_id, int burst, int arrival, int priority, int deadline, int i_o) : p_id(p_id), burst(burst),
                                                                                          arrival(arrival),
                                                                                          schedulerArrival(arrival),
                                                                                          priority(priority),
                                                                                          deadline(deadline),
                                                                                          i_o(i_o),
                                                                                          waitTime(0),
                                                                                          turnaroundTime(0),
                                                                                          timeRan(0),
                                                                                          timePromoted(0),
                                                                                          timeDemoted(0),
                                                                                          endtime(0)
                                                                                          { }



// Deconstructor
Process::~Process() {

}

// Functions

int Process::compareTo(Process* p) {
    int x;
    x = this->schedulerArrival - p->schedulerArrival;
    if (x == 0) {
        x = this->priority - p->priority;
        if (x == 0) {
            // If it is even possible to have a conflict, check and return correctly
            if (this->timeDemoted > 0 && p->timePromoted > 0 || this->timePromoted > 0 && p->timeDemoted > 0) {
                if (this->timePromoted > this->timeDemoted) {
                    if (this->timePromoted == p->timeDemoted) {
                        return -1;
                    }
                } else {
                    if (this->timeDemoted == p->timePromoted) {
                        return 1;
                    }
                }
            }
            // If there isnt a promotion conflict we just compare pid
            x = this->p_id - p->p_id;
        }
    }
    return x;
}

int Process::runAndPrint(int time, int runTime) {
    timeRan = timeRan + runTime;
    int timeLeft = burst - timeRan;

    if (timeRan >= burst) {
        timeLeft = burst - timeRan;
        timeRan = burst;
        runTime = runTime + timeLeft;
        turnaroundTime = time + runTime - arrival;
        // When the process is complete, set the wait time to the current time - the arrival - the burst
        waitTime = (time + runTime) - arrival - burst;
    }
    // Print
    std::cout << "Running Process " << p_id << " from " << time << "s to " << time + runTime << "s\n";

    if (timeRan == burst) {
        std::cout << "Process " << p_id << " has completed!\n";
        std::cout << "This process has waited for a total of " << waitTime << "s!\n";
        std::cout << "This process had a turnaround time of " << turnaroundTime << "s!\n";
    }

    return timeLeft;
}

int Process::runAndPrintWithIO(int time, int runTime) {
    int timeLeft = burst - timeRan;
    bool canDoIO = timeLeft >= runTime;
    //If there is io it is going to run for one less than the quantum
    if (i_o > 0 && canDoIO){
        std::cout << "Process " << p_id << " is doing " << i_o << " clock ticks of I/O\n";
        runTime--;
        i_o = 0;
    }
    timeRan = timeRan + runTime;
    if (timeRan >= burst){
        timeLeft = burst - timeRan;
        timeRan = burst;
        runTime = runTime + timeLeft;
        // When the process is complete, set the wait time to the current time - the arrival - the burst
        waitTime = (time + runTime) - arrival - burst;
    }

    // Print
    std::cout << "Running Process " << p_id << " from " << time << "s to " << time + runTime << "s\n";
    if (timeRan == burst) {
        std::cout << "Process " << p_id << " has completed!\n";
        std::cout << "This process has waited for a total of " << waitTime << "s!\n";
    }

    return timeLeft;
}

int Process::runRealTime(int time) {
    // Keep track of the start
    int start = timeRan;

    // Increment the time ran
    timeRan += time;

    // Find out how much time is left before completion
    int timeLeft = burst - timeRan;

    // Set time ran back if need be
//    if (timeRan > burst) {
//        timeRan = burst;
//    }
    return timeLeft;
}

int Process::run(int time) {
    // Keep track of the start
    int start = timeRan;

    // Increment the time ran
    timeRan += time;

    // Find out how much time is left before completion
    int timeLeft = burst - timeRan;

    //Set time ran back if need be
    if (timeRan > burst) {
        timeRan = burst;
    }
    return timeLeft;
}

void Process::print() {
    std::cout << "P_id: " << p_id << " Burst: " << burst << " Arrival: " << arrival << " Priority: " << priority << " Deadline: " << deadline << " I/O: " << i_o << "\n";
}

// Process ID
int Process::getP_id() const {
    return p_id;
}

void Process::setP_id(int p_id) {
    Process::p_id = p_id;
}


// Burst
int Process::getBurst() const {
    return burst;
}

void Process::setBurst(int burst) {
    Process::burst = burst;
}


// Arrival
int Process::getArrival() const {
    return arrival;
}

void Process::setArrival(int arrival) {
    Process::arrival = arrival;
}

// Priority
int Process::getPriority() const {
    return priority;
}

void Process::setPriority(int priority) {
    Process::priority = priority;
}

// Deadline
int Process::getDeadline() const {
    return deadline;
}

void Process::setDeadline(int deadline) {
    Process::deadline = deadline;
}

// I/O
int Process::getI_o() const {
    return i_o;
}

void Process::setI_o(int i_o) {
    Process::i_o = i_o;
}

// Wait time
int Process::getWaitTime() const {
    return waitTime;
}

void Process::setWaitTime(int waitTime) {
    Process::waitTime = waitTime;
}

void Process::addWaitTime(int waitTime) {
    Process::waitTime += waitTime;
}

// Turnaround time
int Process::getTurnaroundTime() const {
    return turnaroundTime;
}

void Process::setTurnaroundTime(int turnaroundTime) {
    Process::turnaroundTime = turnaroundTime;
}

void Process::setEndTime(int endTime) {
    endtime = endTime;
}

int Process::getTimeRan() const {
    return timeRan;
}

void Process::addTimeRan(int time) {
    timeRan = timeRan + time;
}

void Process::setSchedulerArrival(int time) {
    schedulerArrival = time;
}

int Process::getScehdulerArrival() const {
    return schedulerArrival;
}

void Process::setTimePromoted(int time) {
    timePromoted = time;
}

int Process::getTimePromoted() const {
    return timePromoted;
}

void Process::setTimeDemoted(int time) {
    timeDemoted = time;
}

int Process::getTimeDemoted() const {
    return timeDemoted;
}

double Process::getEndTime() const {
    return endtime;
}



