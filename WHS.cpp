//
// Created by Jacob Schultz on 11/29/17.
//

#include "WHS.h"
#include <map>
#include <iostream>
//#include <tkDecls.h>

using namespace std;

WHS::WHS(const vector<Process *> &processes, int quantum, int ageTime, bool debug) : Scheduler(processes), quantum(quantum),
                                                                                               ageTime(ageTime) {
    if (debug){
#define DEBUG
    }
    WHS::execute();
}

void WHS::execute() {
    int ages = 0;
    int demotions = 0;
    int time = 0;
    bool stop = false;
    //Keep a current index of the asset
    int currentIndex = 0;
    int lastArrivalTime = processes.back()->getArrival();
#ifdef DEBUG
    std::cout << "Running WHS with quantum: " << quantum << " and age time: " << ageTime << endl;
#endif
    while (time < lastArrivalTime || !(priorityQueuesLowBand.empty() && priorityQueuesHighBand.empty())){
        //Get the processes coming in at the current time
        list<Process*> processesAtTime;
        int size = processes.size();
        while (currentIndex < processes.size() && processes[currentIndex]->getArrival() <= time){
            processesAtTime.push_back(processes[currentIndex]);
            currentIndex++;
        }
        //Get the priorities of everything that came in and put it in the map
        while (!processesAtTime.empty()){
            Process* p = processesAtTime.front();
            //get the priority which is also the key of the map
            int priority = p->getPriority();

            //If this priority is already in the queue
            if (containsKey(priority)){
                //Add it to the list for that priority
                map<int, list<Process*>>::iterator it;
                if (priority < 50){
                    it = priorityQueuesLowBand.find(priority);
                } else {
                    it = priorityQueuesHighBand.find(priority);
                }
                list<Process*> l = it->second;
                //Add to the list
                l.push_back(p);
                //Update the list with the new value
                (*it).second = l;
            } else {
                //Add the new priority value to the queue
                list<Process*> l;
                l.push_back(p);
                if (priority < 50){
                    priorityQueuesLowBand.insert(make_pair(priority,l));
                } else {
                    priorityQueuesHighBand.insert(make_pair(priority,l));
                }
            }
            //Remove the one we just added to the map
            processesAtTime.pop_front();
        }
        //AGE
        //Check if we can age
        if (priorityQueuesHighBand.size() + priorityQueuesLowBand.size() >= 10){
            //Find the bottom 10%
            int bottomTen = (priorityQueuesHighBand.size() + priorityQueuesLowBand.size()) / 10;
            //Start at the bottom and age up
            int i = 0;
            int ageCount = 0;
            while (i < 99 && ageCount < bottomTen){
                if (containsKey(i)){
                    //Age processes at key
                    list<Process*> processesAtPriority = getValueFromKey(i);
                    list<Process*>::iterator it = processesAtPriority.begin();
                    while(processesAtPriority.size() > 0 && it != processesAtPriority.end() && (time - (*it)->getScehdulerArrival()) >= ageTime){
                        Process* p = (*it);
                        if (p->getI_o() > 0){
                            ages++;
                            //Thats when it will arrive
                            p->setSchedulerArrival(p->getScehdulerArrival() + ageTime);
                            //Add it to it's new band
                            addToBand(i + p->getI_o(), p);
                            processesAtPriority.erase(it);
                        }
                        it++;
                    }
                    ageCount++;
                } else {
                    i++;
                }
            }
        }
        //RUN
        int i = 99;
        //Find a proccess to run by queue level
        bool ran = false;
        while (i >= 0 && !ran){
            //See if the queue has this priority in it
            if (containsKey(i)){
                //run the front of the queue for the quantum
                list<Process *> listAtLevel = getValueFromKey(i);
                //Run at the front of the list
                Process* p = listAtLevel.front();

                int prevIO = p->getI_o();
                int timeLeft = p->runAndPrintWithIO(time,quantum);
                ran = true;
                int iORan = p->getI_o() - prevIO;
                time += quantum + timeLeft + iORan;
                listAtLevel.pop_front();

                if (timeLeft > 0) {
                    //Check if we should demote it
                    if (i > p->getPriority()){
                        std::cout << "Demoting process " << p->getP_id() << endl;
                        addToBand(i+1, p);
                        demotions++;
                    } else {
                        pushBackTiebreakers(&listAtLevel, p);
                    }
                } else {
                    deleteKeyFromMap(i);
                    //Check if there is anything left for that key
                    if (listAtLevel.size() > 0){
                        if (i < 50){
                            priorityQueuesLowBand.insert(make_pair(i,listAtLevel));
                        } else {
                            priorityQueuesHighBand.insert(make_pair(i,listAtLevel));
                        }
                    }
                }
            } else {
               i--;
            }
        }
        if (!ran && currentIndex < processes.size()) {
            time = processes[currentIndex]->getArrival();
        }
        std::cout << endl;
    }
    std::cout << "Ages: " << ages << endl;
    std::cout << "Demotions: " << demotions << endl;

}


bool WHS::containsKey(int val){
    map<int, list<Process*>>::iterator it;
    if (val < 50){
        it = priorityQueuesLowBand.find(val);
        if (it != priorityQueuesLowBand.end()){
            return true;
        }
    } else {
        it = priorityQueuesHighBand.find(val);
        if (it != priorityQueuesHighBand.end()){
            return true;
        }
    }
    return false;
}

list<Process*> WHS::getValueFromKey(int val){
    map<int, list<Process*>>::iterator it;
    if (val < 50){
        it = priorityQueuesLowBand.find(val);
        if (it != priorityQueuesLowBand.end()){
            return it->second;
        }
    } else {
        it = priorityQueuesHighBand.find(val);
        if (it != priorityQueuesHighBand.end()){
            return it->second;
        }
    }
    return it->second;
}

void WHS::deleteKeyFromMap(int val){
    map<int, list<Process*>>::iterator it;
    if (val < 50){
        it = priorityQueuesLowBand.find(val);
        priorityQueuesLowBand.erase(it);
    } else {
        it = priorityQueuesHighBand.find(val);
        priorityQueuesHighBand.erase(it);
    }
}

void WHS::addToBand(int level, Process* p){
    int originPriority = p->getPriority();
    if (originPriority < 50){
        if (originPriority + level > 49){
            level = 49;
        } else {
            level = originPriority + level;
        }
    } else {
        if (originPriority + level > 99){
            level = 99;
        } else {
            level = originPriority + level;
        }
    }
    if (containsKey(level)){
        //Add it to the list for that priority
        map<int, list<Process*>>::iterator it;
        if (level < 50){
            it = priorityQueuesLowBand.find(level);
        } else {
            it = priorityQueuesHighBand.find(level);
        }
        list<Process*> l = it->second;
        //Add to the list
        if (l.size() == 0) {
            l.push_back(p);
        } else {
            list<Process*>::reverse_iterator it, toInsert;
            toInsert = l.rbegin();
            it = l.rbegin();
            while (it != l.rend() && p->compareTo((*it)) < 0) {
                it++;
            }
            l.insert(it.base(), p);
        }
    } else {
        //Add the new priority value to the queue
        list<Process*> l;
        l.push_back(p);
        if (level < 50){
            priorityQueuesLowBand.insert(make_pair(level,l));
        } else {
            priorityQueuesHighBand.insert(make_pair(level,l));
        }
    }
}

void WHS::pushBackTiebreakers(list<Process*>* listPointer, Process* p) {
    if ((*listPointer).size() == 0) {
        (*listPointer).push_back(p);
    } else {
        list<Process*>::reverse_iterator it, toInsert;
        toInsert = (*listPointer).rbegin();
        it = (*listPointer).rbegin();
        while (it != (*listPointer).rend() && p->compareTo((*it)) < 0) {
            it++;
        }
        (*listPointer).insert(it.base(), p);
    }

}