//
// Created by Peter Quaranta on 11/23/17.
//

#include "RoundRobin.h"
using namespace std;

RoundRobin::RoundRobin(vector<Process*> processes, int quantum) : Scheduler(processes), quantum(quantum) {}

void RoundRobin::execute() {
    // Starting time and current index
    int time = 0;
    int currIndex = 0;

    bool stop = false;

    // This will store our processes to run
    vector <Process*> runQueue;

    while (!stop) {
        int increment = quantum;


        // Get all new arrivals at this time
        vector<Process *> newProcesses = this->getProcess(time);

        // Add all the new processes to beginning of the run queue
        runQueue.insert(runQueue.begin(), newProcesses.begin(), newProcesses.end());

        // Run the next process if there is one
        if (runQueue.size() > 0) {
            // Print
            int pid = runQueue[currIndex]->getP_id();
            std::cout << "\nRunning Process " << pid << " from " << time << "s to ";

            // Run process
            int processTimeLeft = runQueue[currIndex]->run(quantum);

            // If the process has completed
            if (processTimeLeft <= 0) {
                // Change the time to reflect that the full quantum may not have been completed
                increment += processTimeLeft;
                // Remove the process from the run queue
                runQueue.erase(runQueue.begin() + currIndex);
                // Move the index back
                currIndex--;
            }

            // Finish printing and check if process has completed
            std::cout << time + increment << "s\n";
            if (processTimeLeft <= 0) {
                std::cout << "Process " << pid << " has completed!\n";
                runQueue[currIndex]->setEndTime(time+increment);
                //totalWaitTime += runQueue[currIndex]->getWaitTime();
                //std::cout << "Wait time individual: " << runQueue[currIndex]->getWaitTime() << "\n";
                //totalTurnAroundTime += runQueue[currIndex]->getEndTime() - runQueue[currIndex]->getArrival();
                finished.push_back(runQueue[currIndex]);
            }

            // Update the current index in the run queue
            if (runQueue.size() == 0) {
                currIndex = 0;
            } else {
                currIndex = (currIndex + 1) % runQueue.size();
            }
            std::cout << "\n";
        } else {
            // If there are still processes that haven't arrived
            if (this->getProcesses().size() > 0) {
                // Idle until next process comes in
                int nextTime = this->getProcesses()[0]->getArrival();
                int idle = nextTime - time;
                increment = idle;
            } else {
                // We can stop because there are no processes in the run queue and no more processes incoming
                stop = true;
                increment = 0;
            }
        }

        // Increment the time
        time += increment;
    }

}

int RoundRobin::runOnce() {
    int increment = quantum;

    Process* p = runQueue.front();

    // Run process
    int processTimeLeft = p->runAndPrint(currentTime, quantum);
    runQueue.pop_front();

    // If the process has completed
    if (processTimeLeft <= 0) {
        // Change the time to reflect that the full quantum may not have been completed
        increment += processTimeLeft;
        totalWaitTime += p->getWaitTime();
        totalTurnAroundTime += p->getTurnaroundTime();
    } else {
        p->setSchedulerArrival(currentTime + quantum);
        toDemote.push_back(p);
    }

    return increment;
}

vector<Process *> RoundRobin::getFinished() {
    return finished;
}
